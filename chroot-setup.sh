#! /bin/bash

# grub install

read -p 'Install grub EFI application? [Y/n] : ' yn
if ! [[ $yn =~ [Nn] ]]
then
	grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=BOOT
	mv /efi/EFI/BOOT/grubx64.efi /efi/EFI/BOOT/BOOTX64.EFI
fi
echo '[DONE]'
echo

# grub config

read -p 'Generate /boot/grub/grub.cfg? [Y/n] : ' yn
if ! [[ $yn =~ [Nn] ]]
then
	grub-mkconfig -o /boot/grub/grub.cfg
fi
echo '[DONE]'
echo

# set root password -- will prompt for password

read -p 'Set root password? [Y/n] : ' yn
if ! [[ $yn =~ [Nn] ]]
then
	passwd
fi
echo '[DONE]'
echo

# create non-root user

read -p 'Create non-root user? [Y/n] : ' yn
if ! [[ $yn =~ [Nn] ]]
then
	useradd -m -g users -G wheel karsuren	
fi
echo '[DONE]'
echo

# set non-root user's password -- will prompt for password

read -p "Set non-root user's password? [Y/n] : " yn
if ! [[ $yn =~ [Nn] ]]
then
	passwd karsuren
fi
echo '[DONE]'
echo

# setup sudo

read -p 'Setup sudo for non-root user? [Y/n] : ' yn
if ! [[ $yn =~ [Nn] ]]
then
	sed -i 's/^# %wheel ALL=(ALL:ALL) ALL/ %wheel ALL=(ALL:ALL) ALL/' /etc/sudoers
fi
echo '[DONE]'
echo

# nvidia

read -p 'Setup nvidia? [Y/n] : ' yn
if ! [[ $yn =~ [Nn] ]]
then
	mkdir -p /etc/pacman.d/hooks/
	cp /arch-setup/nvidia.hook /etc/pacman.d/hooks/
	cp /arch-setup/grub /etc/default/grub
	sed -i 's/^MODULES=()/MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)/' /etc/mkinitcpio.conf
	sed -i 's/^HOOKS=(base udev/HOOKS=(base systemd/' /etc/mkinitcpio.conf
	echo 'Updating/Generating config files'
fi
echo '[DONE]'
echo

if ! [[ $yn =~ [Nn] ]]
then
	echo 'Regenerating initramfs and grub file to reflect config changes'
	mkinitcpio -P
	grub-mkconfig -o /boot/grub/grub.cfg
fi
echo '[DONE]'
echo

# clone 

read -p 'Clone arch-setup repo to user home? [Y/n] : ' yn
if ! [[ $yn =~ [Nn] ]]
then
	rm -Rf /home/karsuren/arch-setup
	git clone https://gitlab.com/karsuren.coder/arch-setup /home/karsuren/arch-setup
	sudo chown -R karsuren:users /home/karsuren/arch-setup
fi
echo '[DONE]'
echo

# configure dotgit

read -p 'Configure dotgit? [Y/n] : ' yn
if ! [[ $yn =~ [Nn] ]]
then	
	rm -Rf /home/karsuren/.dotgit
	git clone https://gitlab.com/karsuren.coder/dotfiles --bare /home/karsuren/.dotgit
	git --git-dir=/home/karsuren/.dotgit --work-tree=/home/karsuren reset HEAD
	git --git-dir=/home/karsuren/.dotgit --work-tree=/home/karsuren restore /home/karsuren/
	git --git-dir=/home/karsuren/.dotgit --work-tree=/home/karsuren config --local status.showUntrackedFiles no
	sudo chown -R karsuren:users /home/karsuren/.dotgit
	sudo chown -R karsuren:users /home/karsuren/
fi
echo '[DONE]'
echo

# setup audio

echo 'Setting up audio'
amixer sset Master unmute
amixer sset Master 80%
echo '[DONE]'
echo
