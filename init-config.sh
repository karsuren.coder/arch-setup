#! /bin/bash

# configuring git user details

echo 'configuring git user details'
git config --global user.email karsuren.coder@gmail.com
git config --global user.name "Karthik Surendran"
echo '[DONE]'
echo

# enable dhcpcd

echo 'Enabling dhcpcd daemon'
sudo systemctl enable dhcpcd
#sudo systemctl enable dhcpcd@enp3s0
echo '[DONE]'
echo

# setup locale

echo 'Setting up timezone and locale'
sudo sed -i 's/^#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
sudo locale-gen
sudo cp /home/karsuren/arch-setup/locale.conf /etc/
sudo timedatectl set-timezone Asia/Kolkata
sudo timedatectl set-ntp true
sleep 1s
sudo hwclock --systohc
echo '[DONE]'
echo

# cleanup

echo 'Cleaning up temporary arch-setup created in the root folder'
sudo rm -Rf /arch-setup
echo '[DONE]'
echo

# rebooting
echo 'Rebooting system to apply changes'
sleep 3s;sudo reboot
