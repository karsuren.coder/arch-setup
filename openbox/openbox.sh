# Install packages

read -p 'Install necessary pacakages? [Y/n] : ' yn
if ! [[ $yn =~ [Nn] ]]
then
	sudo pacman -S --needed xorg-server xorg-xinit openbox xterm ttf-dejavu ttf-liberation tint2 alacritty xorg-xrandr
fi
echo '[DONE]'
echo

read -p 'Copy xinitrc? [Y/n] : ' yn
if ! [[ $yn =~ [Nn] ]]
then
	cp /home/karsuren/arch-setup/openbox/.xinitrc ~
fi
echo '[DONE]'
echo

read -p 'Enable autologin on tty1? [Y/n] : ' yn
if ! [[ $yn =~ [Nn] ]]
then
	sudo mkdir -p /etc/systemd/system/getty@tty1.service.d/
	sudo cp /home/karsuren/arch-setup/openbox/autologin.conf /etc/systemd/system/getty@tty1.service.d/autologin.conf
fi
echo '[DONE]'
echo
