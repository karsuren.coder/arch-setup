#! /bin/bash

# set a bigger console font

echo 'Setting Bigger Console font'
setfont iso01-12x22
echo '[DONE]'
echo

# configure git

echo 'Configuring git'
git config --global user.email karsuren.coder@gmail.com
git config --global user.name "Karthik Surendran"
echo '[DONE]'
echo

# umounting

echo 'Blind Umounting partitions'
umount /dev/nvme0n1p1
umount /dev/nvme0n1p3
echo '[DONE]'
echo

# format root partitions

read -p 'Format root partition? [Y/n] : ' yn
if ! [[ $yn =~ [nN] ]]
then
	mkfs.ext4 -F /dev/nvme0n1p3
fi
echo '[DONE]'
echo

# format EFI partition

read -p 'Format EFI partition? [Y/n] : ' yn
if ! [[ $yn =~ [nN] ]]
then
	mkfs.fat -F 32 /dev/nvme0n1p1
fi
echo '[DONE]'
echo

# mounting

echo 'Mounting root and efi partitions'
mount /dev/nvme0n1p3 /mnt
mkdir /mnt/efi
mount /dev/nvme0n1p1 /mnt/efi
echo

# pacstrap

read -p 'Install base packages? [Y/n] : ' yn
if ! [[ $yn =~ [nN] ]]
then
	pacstrap /mnt base linux-lts linux-firmware intel-ucode nvidia-lts grub efibootmgr vim sudo git dhcpcd man-db man-pages texinfo alsa-utils
fi
echo '[DONE]'
echo

# genfstab

read -p 'Append genfstab? [Y/n] : ' yn
if ! [[ $yn =~ [nN] ]]
then
	genfstab -U /mnt >> /mnt/etc/fstab
fi
echo '[DONE]'
echo

# cloning arch-setup repo into new linux install

read -p 'Clone arch-setup repo into new linux install? [Y/n] : ' yn
if ! [[ $yn =~ [Nn] ]]
then
	cd /mnt
	git clone https://gitlab.com/karsuren.coder/arch-setup.git
fi
echo '[DONE]'
echo

# chroot

echo 'Chrooting into new linux install'
arch-chroot /mnt 
umount /dev/nvme0n1p2
