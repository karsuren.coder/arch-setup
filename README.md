# Why?

I need to create a fresh arch install with all the my necessary config

This repo will contain all the (mostly) autmatic setup scripts and config files for setting up a fresh arch install for my specific setup

# Should others use it?

It is always too much work to make something work for a lot of people

I don't have time to spent thinking about that problem. So the scripts here are designed to work for my flow

# Instructions

## Speed up pacman downloads

Before the first internet connection attempt, even before installing `git` or cloning repo, setup timezone, ntp and mirrors

The very first thing you should run upon booting arch iso:

```
timedatectl set-timezone Asia/Kolkata
timedatectl set-ntp true
hwclock --systohc
sleep 1m
```

By the time sleep 1m in done the reflectors should be setup, if not wait for that to happen, from now on pacman should be fast

Now clone repo -- `git clone https://gitlab.com/karsuren.coder/arch-setup.git`

then `cd arch-setup`

## run order

1. Ensure pacman mirrors are setup properly
2. setup.sh
3. setup.sh will chroot you into linux install -- now run chroot-setup.sh
4. exit chroot and boot into linux installation
5. enable-dhcp.sh
