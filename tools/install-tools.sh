#! /bin/bash

# yay

read -p 'Install yay? [Y/n] : ' yn

if ! [[ $yn =~ [Nn] ]]
then
	echo 'Installing dependencies'
	sleep 3s
	sudo pacman -S --needed git base-devel
fi
echo '[DONE]'
echo

if ! [[ $yn =~ [Nn] ]]
then
	echo 'Installing yay'
	sleep 3s
	sudo rm -Rf /home/karsuren/yay
	git clone https://aur.archlinux.org/yay.git /home/karsuren/yay
	cd /home/karsuren/yay
	makepkg -si
	cd -
	sudo rm -Rf /home/karsuren/yay
	yay --version
fi
echo '[DONE]'
echo

# Chrome

read -p 'Install google-chrome? [Y/n] : ' yn
if ! [[ $yn =~ [Nn] ]]
then
	yay -S google-chrome
fi
echo '[DONE]'
echo

# ExpressVPN

read -p 'Install express vpn? : ' express_yn

if ! [[ $express_yn =~ [Nn] ]]
then
	sudo pacman -U /home/karsuren/arch-setup/tools/expressvpn-3.19.0.13-1-x86_64.pkg.tar.xz
	sudo systemctl enable expressvpn
fi
echo '[DONE]'
echo

read -p 'erboot? : ' express_yns

if [ $express_yns ]
then
	echo 'Rebooting ...'
	sleep 3s
	sudo reboot
fi
